#include "main.h"
#include "CEP/main_application.h"

void SystemClock_Config();

int main()
{
  HAL_Init();
  CLEAR_BIT(RCC->CFGR, RCC_CFGR_SW);

  /* Configure the system clock */
  SystemClock_Config();

  MainApplication& application = MainApplication::Get();

  application.Init();

  if(application.DoPost()) {
    application.Run();
  }

  __asm("bkpt 1");

  return 0;
}