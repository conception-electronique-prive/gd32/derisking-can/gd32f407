#include "main_application.h"
#include <NilaiTFO/drivers/uartModule.hpp>
#include <can.h>
#include <gpio.h>
#include <rtc.h>
#include <usart.h>

CanModule *s_can[MainApplication::CAN_COUNT] = {};

bool MainApplication::DoPost() {
  m_logger->Log("DoPost\r\n");
  return true;
}

void MainApplication::Init() {
  InitializeHal();
  InitializeModules();
}

void MainApplication::Run() {
  static constexpr uint32_t period = 15000;
  static uint32_t deadline[CAN_COUNT] = {HAL_GetTick(), HAL_GetTick()};
  if (m_logger == nullptr)
    return;
  m_logger->Log("Run\r\n");
  __enable_irq();
  static constexpr CAN_HandleTypeDef *handles[CAN_COUNT] = {
      &hcan1,
      &hcan2,
  };
  for (;;) {
    for (auto i : CAN_ENABLED) {
      for (auto fifo : {CAN_RX_FIFO0, CAN_RX_FIFO1}) {
        auto level = HAL_CAN_GetRxFifoFillLevel(handles[i], fifo);
        if (level != 0) {
          m_logger->Log("CAN%d FIFO%d Rx level %d\r\n", i + 1, fifo, level);
        }
      }
    }

    auto now = HAL_GetTick();

    for (auto i : CAN_ENABLED) {
      m_can[i]->Run();
      if (m_can[i]->GetNumberOfAvailableFrames() != 0) {
        deadline[0] = now + period;
        deadline[1] = now + period;
        auto frame = m_can[i]->ReceiveFrame();
        m_logger->Log("Received: [%d]", frame.frame.StdId);
        for (auto c : frame.data) {
          m_logger->Log(" %02x", c);
        }
        m_logger->Log("\r\n");
        if (frame.data.size() == 8 && frame.data[0] == 1 &&
            frame.data[1] == 2 && frame.data[2] == 3 && frame.data[3] == 4 &&
            frame.data[4] == 5 && frame.data[5] == 6 && frame.data[6] == 7 &&
            frame.data[7] == 8) {
          m_logger->Log("Repeat\r\n");
          m_can[i]->TransmitFrame(frame.frame.StdId, frame.data.data(),
                                  frame.data.size());
        }
      }
    }
  }
}

MainApplication &MainApplication::Get() {
  static MainApplication instance;
  return instance;
}

void MainApplication::InitializeHal() {
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_USART2_UART_Init();
  MX_CAN1_Init();
  MX_CAN2_Init();
}

void MainApplication::InitializeModules() {
  m_logger = new Logger(new UartModule(&huart2, "uart2"));
  m_logger->Log("\033c");
  m_logger->Log("GD32\r\n");

  new RtcModule(&hrtc, "rtc");

  for (auto i : CAN_ENABLED) {
    static constexpr CAN_HandleTypeDef *handles[CAN_COUNT] = {
        &hcan1,
        &hcan2,
    };
    static constexpr const std::string_view labels[CAN_COUNT] = {
        "can1",
        "can2",
    };
    m_can[i] = new CanModule(handles[i], labels[i].data());
    s_can[i] = m_can[i];

    if (handles[i]->State != HAL_CAN_STATE_READY &&
        handles[i]->State != HAL_CAN_STATE_LISTENING)
      continue;

    m_can[i]->EnableInterrupts({
        CEP_CAN::Irq::TxMailboxEmpty,
        CEP_CAN::Irq::Fifo0MessagePending,
        CEP_CAN::Irq::Fifo0Full,
        CEP_CAN::Irq::Fifo0Overrun,
        CEP_CAN::Irq::Fifo1MessagePending,
        CEP_CAN::Irq::Fifo1Full,
        CEP_CAN::Irq::Fifo1Overrun,
        CEP_CAN::Irq::Wakeup,
        CEP_CAN::Irq::SleepAck,
        CEP_CAN::Irq::ErrorWarning,
        CEP_CAN::Irq::ErrorPassive,
        CEP_CAN::Irq::BusOffError,
        CEP_CAN::Irq::LastErrorCode,
        CEP_CAN::Irq::ErrorStatus,
    });

    CAN_FilterTypeDef filter;
    filter.FilterIdLow = 0;
    filter.FilterIdHigh = 0;
    filter.FilterMaskIdLow = 0;
    filter.FilterMaskIdHigh = 0;
    filter.FilterBank = 14 * i;
    filter.FilterMode = CAN_FILTERMODE_IDMASK;
    filter.FilterScale = CAN_FILTERSCALE_32BIT;
    filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
    filter.FilterActivation = CAN_FILTER_ENABLE;
    HAL_CAN_ConfigFilter(handles[0], &filter);

    // IRQ enable after everything is initialized to prevent calling interrupts
    // without handlers ready.
    static constexpr decltype(CAN1_RX0_IRQn) CANX_RX0_IRQn[CAN_COUNT] = {
        CAN1_RX0_IRQn,
        CAN2_RX0_IRQn,
    };
    static constexpr decltype(CAN1_RX1_IRQn) CANX_RX1_IRQn[CAN_COUNT] = {
        CAN1_RX1_IRQn,
        CAN2_RX1_IRQn,
    };
    static constexpr decltype(CAN1_TX_IRQn) CANX_TX_IRQn[CAN_COUNT] = {
        CAN1_TX_IRQn,
        CAN2_TX_IRQn,
    };
    static constexpr decltype(CAN1_SCE_IRQn) CANX_SCE_IRQn[CAN_COUNT] = {
        CAN1_SCE_IRQn,
        CAN2_SCE_IRQn,
    };
    HAL_NVIC_EnableIRQ(CANX_RX0_IRQn[i]);
    HAL_NVIC_EnableIRQ(CANX_RX1_IRQn[i]);
    HAL_NVIC_EnableIRQ(CANX_TX_IRQn[i]);
    HAL_NVIC_EnableIRQ(CANX_SCE_IRQn[i]);
  }

  CAN_FilterTypeDef filter;
  filter.FilterBank = 1;
  filter.FilterActivation = CAN_FILTER_DISABLE;
  filter.SlaveStartFilterBank = 13;
  HAL_CAN_ConfigFilter(&hcan1, &filter);
}

extern "C" {
void Can1Callback() { s_can[0]->HandleIrq(); }
void Can2Callback() { s_can[1]->HandleIrq(); }
}