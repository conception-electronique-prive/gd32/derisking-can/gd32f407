#ifndef GD32_CAN_MAIN_APPLICATION_H
#define GD32_CAN_MAIN_APPLICATION_H

#include <NilaiTFO/defines/module.hpp>
#include <NilaiTFO/drivers/canModule.hpp>
#include <NilaiTFO/processes/application.hpp>
#include <NilaiTFO/services/logger.hpp>

#include <map>

class MainApplication final : public cep::Application {
private:
  MainApplication() = default;
  ~MainApplication() final = default;

public:
  bool DoPost() final;
  void Init() final;
  void Run() final;
  static MainApplication &Get();

private:
  static void InitializeHal();
  void InitializeModules();

public:
  static constexpr std::size_t CAN_COUNT = 2;
  std::vector<std::size_t> CAN_ENABLED = {0, 1};

private:
  Logger *m_logger = nullptr;
  CanModule *m_can[CAN_COUNT] = {};
};

#endif // GD32_CAN_MAIN_APPLICATION_H
