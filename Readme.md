# Derisking CAN GD32

Ce projet a été utilisé pour valider le bon fonctionnement du CAN avec le GD32F405.

Il a été testé avec une carte GD32507R-Start programmée avec le programme ci-present, reliée à une carte Vision21
programmée avec le [projet lié](https://gitlab.com/conception-electronique-prive/gd32/derisking-can/stm32l452)

Le but du programme et que dès qu'une carte reçoit une trame valid, elle la retransmette immédiatement.
Si le CAN est défaillant, alors cette trame finira par s'arreter.

Pour le contrôler, on a branché les sondes d'un analyseur logique sur les broches RX/TX CAN de chaque cartes.
Le test a été roulé pendant deux minutes, et l'échange a su rester.

![logic](resources/logic.png)

En revanche, il apparait que les procedures d'initialisation du GD32 ne sont pas les même que celle du STM32.

## Problèmes rencontrés

### Blocage initialisation

Le CAN du GD32 n'est pas capable de s'initialiser.
La requete d'initialisation n'est jamais acceptée par le MCU, et la fonction échoue par un timeout.
Avec le SMT32, l'opération consiste à faire une requete d'initialisation, vérifier qu'elle a été acceptée puis reveiller
le controleur.
Avec le GD32, l'opération consiste à réveiller le controleur, faire une requete d'initialisation et vérifier qu'elle a
été acceptée.

```C
/** stm32f4xx_hal_can.c **/

// Sleep must be exited here with the GD32
CLEAR_BIT(hcan->Instance->MCR, CAN_MCR_SLEEP);

/* Request initialisation */
SET_BIT(hcan->Instance->MCR, CAN_MCR_INRQ);

/* Get tick */
tickstart = HAL_GetTick();

/* Wait initialisation acknowledge */
while ((hcan->Instance->MSR & CAN_MSR_INAK) == 0U)
{
    if ((HAL_GetTick() - tickstart) > CAN_TIMEOUT_VALUE)
    {
      /* Update error code */
      hcan->ErrorCode |= HAL_CAN_ERROR_TIMEOUT;
    
      /* Change CAN state */
      hcan->State = HAL_CAN_STATE_ERROR;
    
      return HAL_ERROR;
    }
}

// On HAL 1.27.1, sleep is exited here
// /* Exit from sleep mode */
// CLEAR_BIT(hcan->Instance->MCR, CAN_MCR_SLEEP);
```
